import React, {Component} from 'react';
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import styles from './index.css'

@connect(({app}) => ({app}))
class AdminNavigation extends Component {

  render() {


    return (
      <div className={styles.body}>
        <div className="container">
          <nav className="navbar navbar-expand-md ">
            <div className="collapse navbar-collapse d-flex justify-content-between">
              <Link className="navbar-brand pr-0" to="/">
                <img className={styles.logo} src="/assets/images/logo1.png" alt=""/>
              </Link>
              <ul className="navbar-nav d-flex justify-content-around">
                <li className="nav-item">
                  <Link className="nav-link active " to="/cabinet"><span className={styles.text}>KASBGA YO'NALTIRISH</span></Link>
                </li>
                <li className="mx-1 nav-item">
                  <Link className="nav-link " to="/auth/register"><span className={styles.text}>ARIZA TOPSHIRISH</span></Link>
                </li>
                <li className="mx-1 nav-item">
                  <Link className="nav-link " to="/catalog"><span className={styles.text}>STATISTIKA</span></Link>
                </li>
                <li className="mx-1 nav-item">
                  <Link className="nav-link " to="/company"><span className={styles.text}>YO'RIQNOMA</span></Link>
                </li>
                <li className="mx-1 nav-item">
                  <Link className="nav-link " to="/company"><span className={styles.text}>BOG'LANISH</span></Link>
                </li>
                <li className="mx-1 nav-item">
                  <Link className="nav-link " to="/auth/login"><span className={styles.text}>KIRISH</span></Link>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </div>
    );
  }
}

AdminNavigation.propTypes = {};

export default AdminNavigation;
