import "bootstrap/dist/css/bootstrap.min.css"
import AdminNavigation from "../component/AdminNavigation";
import styles from './index.css'
function BasicLayout(props) {
  return (
    <div className={styles.body}>
      <div className={styles.background}>
        <AdminNavigation/>
      </div>
      {props.children}
    </div>
  );
}

export default BasicLayout;
