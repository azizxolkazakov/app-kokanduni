import {STORAGE_NAME} from 'utils/constant';
import api from 'api'
import {config} from 'utils';
import router from 'umi/router';

const {
  userMe
} = api;


export default {
  namespace: 'app',
  state: {

  },
  subscriptions: {

  },
  effects: {
    * userMe({payload}, {call, put, select}) {
      try {
        const res = yield call(userMe);
        if (!res.success) {
          yield put({
            type: 'updateState',
            payload: {
              currentUser: {}
            }
          });
          localStorage.removeItem(STORAGE_NAME);
          router.push('/auth/login');
          if (payload) {
            if (!config.openPages.includes(payload.pathname)) {
              localStorage.removeItem(STORAGE_NAME);
              router.push('/auth/login')
            }
          } else {
            localStorage.removeItem(STORAGE_NAME);
            router.push('/auth/login')
          }
        } else {
          yield put({
            type: 'updateState',
            payload: {
              currentUser: res.object
            }
          });
          yield put({
            type: 'getMyCompanies'
          })
        }
      } catch (error) {
        yield put({
          type: 'updateState',
          payload: {currentUser: {}}
        });
        localStorage.removeItem(STORAGE_NAME);
        router.push('/auth/login');
        if (payload) {
          if (!config.openPages.includes(payload.pathname) && !payload.pathname.includes('/article/') && !payload.pathname.includes('/videos/')) {
            localStorage.removeItem(STORAGE_NAME);
            router.push('/auth/login')
          }
        } else {
          localStorage.removeItem(STORAGE_NAME);
          router.push('/auth/login')
        }
      }
    },

  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }

}
