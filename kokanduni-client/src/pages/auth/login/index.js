import {STORAGE_NAME} from 'utils/constant';
import React, {Component} from 'react';
import {Button, Card, Col, Container, Input, Row} from "reactstrap";
import router from "umi/router";
import {connect} from "dva";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {Link} from "react-router-dom";
import styles from './index.css'


@connect(({auth}) => ({auth}))
class Index extends Component {
  componentDidMount() {
    const {dispatch} = this.props;
    if (localStorage.getItem(STORAGE_NAME)) {
      dispatch({
        type: 'auth/userMe'
      }).then(r => {
        if (r.success) {
          router.push('/');
        }
      })
    }
  }

  render() {
    const {dispatch, auth} = this.props;

    const login = (e, v) => {
      console.log(v);
      dispatch({
        type: 'auth/signIn',
        payload: {
          ...v
        }
      })
    };

    return (
      <div className={styles.box}>
        <Container>
          <Row className="mt-5">
            <Col md={4} className="offset-4">
              <div className={styles.form}>
                <Row>
                  <Col md={8} className="offset-2">
                    <img className={styles.logo} src="/assets/images/kokanduni.png" alt=""/>
                  </Col>
                </Row>
                <AvForm onValidSubmit={login}>
              <Row className="mt-2">
                <Col>
                    <AvField type="text" className={styles.input}  placeholder="PHONE NUMBER" name="phoneNumber" />
                    <AvField type="password" className={styles.input} placeholder="PASSWORD" name="password" />
                </Col>
              </Row>
                <Row className="mt-3">
                  <Col md={5}>
                    <Link to="#" className={styles.texts}><i>Unutib qo'ydim</i></Link>
                  </Col>
                  <Col md={7}>
                    <button type="submit" className={styles.button}>KIRISH</button>
                  </Col>
                </Row>
                </AvForm>
              </div>
              <div className={styles.float}>
                <Link to="/auth/register" className={styles.text}>Ariza topshirish</Link>
              </div>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

Index.propTypes = {};

export default Index;
