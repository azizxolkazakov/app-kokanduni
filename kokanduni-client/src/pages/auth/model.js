import api from 'api'
import {STORAGE_NAME} from 'utils/constant';
import {router} from 'umi';

const {
  signIn, getPhoto, userMe, signUp, addPhoto, getCountries, getCountry, getRegions, getRegion, getDistricts, getDistrict,
  getGenders, getGender, getNations, getNation, getCareers, getCareer, getTypeStudies, getTypeStudy,
  getDirections, getDirection, getLanguageEducations, getLanguageEducation, getForeignLanguages,
  getForeignLanguage,exportPdf

} = api;

export default ({
  namespace: 'auth',
  state: {
    countries: [],
    country: '',
    regions: [],
    region: '',
    districts: [],
    district: '',
    genders: [],
    gender: '',
    nations: [],
    nation: '',
    careers: [],
    career: '',
    typeStudies: [],
    typeStudy: '',
    directions: [],
    direction: '',
    languageEducations: [],
    languageEducation: '',
    foreignLanguages: [],
    foreignLanguage: '',
    passportPdfId: '',
    photoId: '',
    infoUser: {},
    show1: true,
    show2: false,
    show3: false,
    show4: false,
    photo: '',
    reqUser: {},
    photoSize: 0,
    password: ''
  },
  subscriptions: {},
  effects: {
    *exportPdf({payload},{call}){
      const res= yield call(exportPdf);
    },

    * userMe({payload}, {call, put, select}) {
      const res = yield call(userMe);
      return res;
    },
    * signIn({payload}, {call, put, select}) {
      console.log(payload)
      const res = yield call(signIn, payload);
      console.log(res)
      if (res.success) {
        localStorage.setItem(STORAGE_NAME, res.tokenType + " " + res.tokenBody);
        router.push('/catalog/employee')
      }
    },

    * signUp({payload}, {call, put, select}) {
      const res = yield call(signUp, payload);
       if (res.success){
         yield put({
           type: 'updateState',
           payload: {
             password: res.object
           }
         });
         yield put({
           type: 'auth/updateState',
           payload: {
             show3:false
           }
         })
       }
    },
    * addPhoto({payload}, {call, put, select}) {
      return yield call(addPhoto, {payload});
    },
    * getCountries({}, {call, put}) {
      const res = yield call(getCountries);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            countries: res._embedded.countries
          }
        })
      }
    },
    * getRegions({}, {call, put}) {
      const res = yield call(getRegions)
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            regions: res._embedded.regions
          }
        })
      }
    },
    * getDistricts({}, {call, put}) {
      const res = yield call(getDistricts)
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            districts: res._embedded.districts
          }
        })
      }
    },
    * getGenders({}, {call, put}) {
      const res = yield call(getGenders);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            genders: res._embedded.genders
          }
        })
      }
    },
    * getNations({}, {call, put}) {
      const res = yield call(getNations)
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            nations: res._embedded.nations
          }
        })
      }
    },
    * getCareers({}, {call, put}) {
      const res = yield call(getCareers)
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            careers: res._embedded.careers
          }
        })
      }
    },
    * getTypeStudies({}, {call, put}) {
      const res = yield call(getTypeStudies);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            typeStudies: res._embedded.typeStudies
          }
        })
      }
    },
    * getDirections({}, {call, put}) {
      const res = yield call(getDirections);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            directions: res._embedded.directions
          }
        })
      }
    },
    * getLanguageEducations({}, {call, put}) {
      const res = yield call(getLanguageEducations);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            languageEducations: res._embedded.languageEducations
          }
        })
      }
    },
    * getForeignLanguages({}, {call, put}) {
      const res = yield call(getForeignLanguages);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            foreignLanguages: res._embedded.foreignLanguages
          }
        })
      }
    },


    * getCountry({payload}, {call, put}) {
      const res = yield call(getCountry, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            country: res
          }
        })
      }
    },
    * getRegion({payload}, {call, put}) {
      const res = yield call(getRegion, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            region: res
          }
        })
      }
    },
    * getDistrict({payload}, {call, put}) {
      const res = yield call(getDistrict, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            district: res
          }
        })
      }
    },
    * getGender({payload}, {call, put}) {
      const res = yield call(getGender, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            gender: res
          }
        })
      }
    },
    * getNation({payload}, {call, put}) {
      const res = yield call(getNation, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            nation: res
          }
        })
      }
    },
    * getCareer({payload}, {call, put}) {
      const res = yield call(getCareer, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            career: res
          }
        })
      }
    },
    * getTypeStudy({payload}, {call, put}) {
      const res = yield call(getTypeStudy, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            typeStudy: res
          }
        })
      }
    },
    * getDirection({payload}, {call, put}) {
      const res = yield call(getDirection, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            direction: res
          }
        })
      }
    },
    * getLanguageEducation({payload}, {call, put}) {
      const res = yield call(getLanguageEducation, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            languageEducation: res
          }
        })
      }
    },
    * getForeignLanguage({payload}, {call, put}) {
      const res = yield call(getForeignLanguage, payload);
      if (res.success) {
        yield put({
          type: 'updateState',
          payload: {
            foreignLanguage: res
          }
        })
      }
    },

  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
