import React, {Component} from 'react';
import {Button, Card, Col, Container, Label, Row} from "reactstrap";
import {AvField, AvForm} from "availity-reactstrap-validation";
import {connect} from "react-redux";
import styles from "./index.css";
import {Link} from "react-router-dom";

@connect(({auth}) => ({auth}))
class Register extends Component {

  componentDidMount() {
    const {dispatch} = this.props;
    dispatch({
      type: 'auth/getCountries'
    });
    dispatch({
      type: 'auth/getRegions'
    });
    dispatch({
      type: 'auth/getDistricts'
    });
    dispatch({
      type: 'auth/getGenders'
    });
    dispatch({
      type: 'auth/getNations'
    });
    dispatch({
      type: 'auth/getCareers'
    });
    dispatch({
      type: 'auth/getTypeStudies'
    });
    dispatch({
      type: 'auth/getDirections'
    });
    dispatch({
      type: 'auth/getLanguageEducations'
    });
    dispatch({
      type: 'auth/getForeignLanguages'
    });

  }

  render() {
    const {dispatch, auth} = this.props;
    const {
      show1, show2, infoUser, show3, countries, country, regions, region,
      districts, district, genders, gender, nations, nation, careers, career,
      typeStudies, typeStudy, directions, direction, languageEducations, languageEducation,
      foreignLanguages, foreignLanguage, password, passportPdfId, photoId, photoSize,show4
    } = auth;

    const registerState = (e, v) => {
      let reqPassportInfo = {...v, passportPdfId};
      let reqInformation = {...v};
      let reqCommunication = {...v};
      let infoUser = {...v, photoId, reqPassportInfo, reqInformation, reqCommunication};
      dispatch({
        type: 'auth/getCountry',
        payload: {
          path: infoUser.reqPassportInfo.countryId
        }
      });
      dispatch({
        type: 'auth/getRegion',
        payload: {
          path: infoUser.regionId
        }
      });
      dispatch({
        type: 'auth/getDistrict',
        payload: {
          path: infoUser.reqPassportInfo.districtId
        }
      });
      dispatch({
        type: 'auth/getGender',
        payload: {
          path: infoUser.genderId
        }
      });
      dispatch({
        type: 'auth/getNation',
        payload: {
          path: infoUser.nationId
        }
      });
      dispatch({
        type: 'auth/getCareer',
        payload: {
          path: infoUser.careerId
        }
      });
      dispatch({
        type: 'auth/getDirection',
        payload: {
          path: infoUser.directorId
        }
      });
      dispatch({
        type: 'auth/getLanguageEducation',
        payload: {
          path: infoUser.languageEducationId
        }
      });
      dispatch({
        type: 'auth/getForeignLanguage',
        payload: {
          path: infoUser.foreignLanguageId
        }
      });

      dispatch({
        type: 'auth/getTypeStudy',
        payload: {
          path: infoUser.typeStudyId
        }
      });
      dispatch({
        type: 'auth/updateState',
        payload: {
          infoUser: infoUser
        }
      });
      dispatch({
        type: 'auth/updateState',
        payload: {
          show2: !show2,
          show1: !show1
        }
      });

    };
    const uploadLogo = (e) => {
      let typeState = e.target.name;

      dispatch({
        type: 'auth/updateState',
        payload: {
          photoSize: e.target.files[0].size
        }
      });
      if (e.target.files[0]) {
        dispatch({
          type: 'auth/addPhoto',
          payload: {
            fileUpload: true,
            file: e.target.files,
            type: 'AVATAR',
          }
        }).then(res => {
          if (res.success) {
            if (typeState === "photoId") {
              dispatch({
                type: 'auth/updateState',
                payload: {
                  photoId: res.data
                }
              })
            } else {
              dispatch({
                type: 'auth/updateState',
                payload: {
                  passportPdfId: res.data
                }
              })
            }
          }
        })
      }
    };


    const nextPage2 = () => {
      dispatch({
        type: 'auth/updateState',
        payload: {
          show2: !show2,
          show3: !show3
        }
      });
    };
    const lastPage = () => {
      dispatch({
        type: 'auth/updateState',
        payload: {
          show1: !show1,
          show2: !show2
        }
      });

    };
    const signUp = (e, v) => {
      let reqUser = {
        ...v,
        lastName: infoUser.lastName,
        middleName: infoUser.middleName,
        firstName: infoUser.firstName,
        birthDate: infoUser.birthDate,
        genderId: infoUser.genderId,
        photoId: photoId,
        reqPassportInfo: infoUser.reqPassportInfo,
        reqInformation: infoUser.reqInformation,
        reqCommunication: infoUser.reqCommunication,
      };
      dispatch({
        type:'auth/signUp',
        payload:{
          ...reqUser
        }
      })
      // dispatch({
      //   type: 'auth/updateState',
      //   payload: {
      //     show3:!show3
      //   }
      // })
    };

    return (
      <div className={styles.body}>
        {show1 ? <div className={styles.list}>
          <Container className={styles.container}>
            <AvForm onValidSubmit={registerState}>
              <Row>
                <Col md={4} className="offset-4">
                  <h4 className="text-center mt-4">Ariza topshirish</h4>
                  <div className="line">
                  </div>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col md={4} className="d-flex justify-content-center">
                  <div>
                    <div className="d-flex justify-content-center">
                      <div className={styles.circleActive}><span>1</span></div>
                    </div>
                    <p>Arizani to'ldirish</p>
                    <div></div>
                  </div>
                </Col>
                <Col md={4} className="d-flex justify-content-center">
                  <div>
                    <div className="d-flex justify-content-center">
                      <div className={styles.circle}><span>2</span></div>
                    </div>
                    <p>Yo'nalishni tanlash</p>
                    <div></div>
                  </div>
                </Col>
                <Col md={4} className="d-flex justify-content-center">
                  <div>
                    <div className="d-flex justify-content-center">
                      <div className={styles.circle}><span>3</span></div>
                    </div>
                    <p>Tasdiqlash</p>
                    <div></div>
                  </div>
                </Col>
              </Row>
              <Row className="d-flex justify-content-center">
                <div><h5>Shaxsiy ma'umotlar</h5></div>
              </Row>
              <Row>
                <Col>
                  <Row className="px-5">
                    <Col md={6}>
                      <Label>Familya <span className="text-danger">*</span></Label>
                      <AvField name="lastName"/>
                      <Label>Otasining ismi<span className="text-danger">*</span></Label>
                      <AvField name="middleName"/>
                      <Label>Jinsi<span className="text-danger">*</span></Label>
                      <AvField type="select" name="genderId">
                        <option value="">Select gender</option>
                        {
                          genders.map(item =>
                            <option key={item.id} value={item.id}>{item.nameUz}</option>
                          )
                        }
                      </AvField>
                      <Label>Fotosurat<span className="text-danger">*</span></Label>
                      <AvField type="file" name="photoId" onChange={uploadLogo}/>
                    </Col>
                    <Col md={6}>
                      <Label>Ism <span className="text-danger">*</span></Label>
                      <AvField name="firstName"/>
                      <Label>Tug'ilgan sanasi <span className="text-danger">*</span></Label>
                      <AvField type="date" name="birthDate"/>
                      <Row>
                        <Col md={8} className="pr-0">
                          <p className={styles.text}>Fotosurat 3.5x4.5 sm bo'lib hajmi 5Mb dan oshmasligi va jpeg,jpg
                            va
                            png formatda bo'lishi kerak. <br/>
                            600x800 piksel o'lchami tavsiya qilinadi. <br/>
                            BIR NECHTALIK SURAT YUKLAMANG
                          </p>
                        </Col>
                        <Col md={4} className="p-0">
                          <div className={styles.person}>
                            <img className="w-100 h-100" src="/assets/images/person.jpg" alt="Shaxsiy rasm"/>
                          </div>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <div className={styles.border}></div>
              <Row className="d-flex justify-content-center mt-4">
                <div><h5>Passport ma'lumotlari</h5></div>
              </Row>
              <Row>
                <Col>
                  <Row className="px-5">
                    <Col md={6}>
                      <Label>Fuqaroligi<span className="text-danger">*</span></Label>
                      <AvField type="select" name="countryId">
                        <option value="">Select country</option>
                        {
                          countries.map(item =>
                            <option key={item.id} value={item.id}>{item.nameUz}</option>
                          )
                        }
                      </AvField>
                      <Label>Hududi<span className="text-danger">*</span></Label>
                      <AvField type="select" name="regionId">
                        <option value="">Select region</option>
                        {
                          regions.map(item =>
                            <option key={item.id} value={item.id}>{item.nameUz}</option>
                          )
                        }
                      </AvField>
                      <Label>Ko'cha nomi,uy va xonadon raqami<span className="text-danger">*</span></Label>
                      <AvField name="homeAddress"/>
                      <Label>Kim tomonidan berilgan<span className="text-danger">*</span></Label>
                      <AvField name="passportAddress"/>
                    </Col>
                    <Col md={6}>
                      <Label>Millati<span className="text-danger">*</span></Label>
                      <AvField type="select" name="nationId">
                        <option value="">Select nation</option>
                        {
                          nations.map(item =>
                            <option key={item.id} value={item.id}>{item.nameUz}</option>
                          )
                        }
                      </AvField>
                      <Label>Tuman<span className="text-danger">*</span></Label>
                      <AvField type="select" name="districtId">
                        <option value="">Select district</option>
                        {
                          districts.map(item =>
                            <option key={item.id} value={item.id}>{item.nameUz}</option>
                          )
                        }

                      </AvField>
                      <Label>Passport yoki guvohnoma seriya va raqami<span className="text-danger">*</span></Label>
                      <AvField type="text" name="passportSeries"/>
                      <Label>Passport yoki guvohnomani pdf shaklida yuklash<span
                        className="text-danger">*</span></Label>
                      <AvField type="file" name="passportPdfId" onChange={uploadLogo}/>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <div className={styles.border}></div>
              <Row className="d-flex justify-content-center mt-4">
                <div><h5>Ma'lumoti</h5></div>
              </Row>
              <Row>
                <Col>
                  <Row className="px-5">
                    <Col md={6}>
                      <Label>Ma'lumoti<span className="text-danger">*</span></Label>
                      <AvField type="select" name="careerId">
                        <option value="">Select careers</option>
                        {
                          careers.map(item =>
                            <option key={item.id} value={item.id}>{item.nameUz}</option>
                          )
                        }
                      </AvField>
                      <Label>O'quv yurti nomi <span className="text-danger">*</span></Label>
                      <AvField name="studyName"/>
                      <Label>Tugatgan muassasi joylashgan viloyat<span className="text-danger">*</span></Label>
                      <AvField type="select" name="regionIdInfo">
                        <option value="">Select region</option>
                        {
                          regions.map(item =>
                            <option key={item.id} value={item.id}>{item.nameUz}</option>
                          )
                        }
                      </AvField>
                    </Col>
                    <Col md={6}>
                      <Label>O'quv yurti turi<span className="text-danger">*</span></Label>
                      <AvField type="select" name="typeStudyId">
                        <option value="">Select study type</option>
                        {
                          typeStudies.map(item =>
                            <option key={item.id} value={item.id}>{item.nameUz}</option>
                          )
                        }
                      </AvField>
                      <Label>O'quv yurtini tugatgan yili<span className="text-danger">*</span></Label>
                      <AvField name="endStudyYear"/>
                      <Label>Tuman<span className="text-danger">*</span></Label>
                      <AvField type="select" name="districtIdInfo">
                        <option value="">Select district</option>
                        {
                          districts.map(item =>
                            <option key={item.id} value={item.id}>{item.nameUz}</option>
                          )
                        }
                      </AvField>
                    </Col>
                  </Row>
                </Col>
              </Row>

              <div className={styles.border}></div>
              <Row className="d-flex justify-content-center mt-4">
                <div><h5>Bog'lanish uchun ma'lumotlar</h5></div>
              </Row>
              <Row>
                <Col>
                  <Row className="px-5">
                    <Col md={6}>
                      <Label>Uyali telefon raqami <span className="text-danger">*</span></Label>
                      <AvField name="phoneNumber"/>
                      <Label>E-mail manzili <span className="text-danger">*</span></Label>
                      <AvField name="email"/>
                    </Col>
                    <Col md={6}>
                      <Label>Qo'shimcha telefon raqami <span className="text-danger">*</span></Label>
                      <AvField name="homePhoneNumber"/>
                    </Col>
                  </Row>
                </Col>
              </Row>
              <Row className='mt-4 d-flex justify-content-center'>
                <button type="submit" className={styles.button}>KEYINGISI</button>
              </Row>
              <Row className="mt-2 ">
                <Col className="pl-5">
                  <span className="text-danger">*</span> <span>-to'ldirilishi shart bo'lgan maydonlar</span>
                </Col>
              </Row>
            </AvForm>
          </Container>
        </div> : show2 ? <div className={styles.list}>
            <Container className={styles.container}>
              <Row>
                <Col md={4} className="offset-4">
                  <h4 className="text-center mt-4">Ariza topshirish</h4>
                  <div className="line"></div>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col md={4} className="d-flex justify-content-center">
                  <div>
                    <div className="d-flex justify-content-center">
                      <div className={styles.circleActive}><span>1</span></div>
                    </div>
                    <p>Arizani to'ldirish</p>
                    <div></div>
                  </div>
                </Col>
                <Col md={4} className="d-flex justify-content-center">
                  <div>
                    <div className="d-flex justify-content-center">
                      <div className={styles.circle}><span>2</span></div>
                    </div>
                    <p>Yo'nalishni tanlash</p>
                    <div></div>
                  </div>
                </Col>
                <Col md={4} className="d-flex justify-content-center">
                  <div>
                    <div className="d-flex justify-content-center">
                      <div className={styles.circle}><span>3</span></div>
                    </div>
                    <p>Tasdiqlash</p>
                    <div></div>
                  </div>
                </Col>
              </Row>
              <Row className="mt-5">
                <Col className="pl-5">
                  <p className="pb-0 mb-0">Familyasi:</p>
                  <p>{infoUser.lastName}</p>
                  <p className="pb-0 mb-0">Ismi:</p>
                  <p>{infoUser.firstName}</p>
                  <p className="pb-0">Otasining ismi:</p>
                  <p>{infoUser.middleName}</p>
                  <p className="pb-0">Tug'ilgan sanasi:</p>
                  <p>{infoUser.birthDate}</p>
                  <p className="pb-0">Familyasi:</p>
                  <p>{infoUser.lastName}</p>
                  <p className="pb-0">Jinsi:</p>
                  <p>{gender.nameUz}</p>
                  <p className="pb-0">Fotosurati:</p>
                  <div>
                    <img className="img-fluid w-50 h-50" src={`/api/attach/${photoId}`} alt=""/>
                  </div>
                  <div className={styles.border}></div>
                  <h6>Passport ma'lumotlari</h6>
                  <p className="pb-0">Fuqaroligi:</p>
                  <p>{country.nameUz}</p>
                  <p className="pb-0">Millati:</p>
                  <p>{nation.nameUz}</p>
                  <p className="pb-0">Hudui:</p>
                  <p>{region.nameUz}</p>
                  <p className="pb-0">Tuman:</p>
                  <p>{district.nameUz}</p>
                  <p className="pb-0">Ko'cha nomi, uy xonadon raqami:</p>
                  <p>{infoUser.reqPassportInfo.homeAddress}</p>
                  <p className="pb-0">Passport yoki guvohnoma seria raqami:</p>
                  <p>{infoUser.reqPassportInfo.passportSeries}</p>
                  <p className="pb-0">Kim tomonidan berilgan:</p>
                  <p>{infoUser.reqPassportInfo.passportAddress}</p>
                  <p className="pb-0">Passport yoki guvohnomani pdf formati:</p>
                  <p>Biriktirilgan hajmi</p>
                  <p> {photoSize} bayt</p>
                  <div className={styles.border}></div>
                  <p>Ma'lumoti</p>
                  <p>{career.nameUz}</p>
                  <p>O'quv yurti turi</p>
                  <p>{typeStudy.nameUz}</p>
                  <p>O'quv yurti nomi</p>
                  <p>{infoUser.reqInformation.studyName}</p>
                  <p>Joylashgan hududi</p>
                  <p>{region.nameUz} shahri {district.nameUz} tumani</p>
                  <p>Tugatgan yili</p>
                  <p>{infoUser.reqInformation.endStudyYear}</p>
                  <div className={styles.border}></div>
                  <p>Uyali telefon raqami</p>
                  <p>{infoUser.reqCommunication.phoneNumber}</p>
                  <p>Qo'shimcha telefon raqami</p>
                  <p>{infoUser.reqCommunication.homePhoneNumber}</p>
                  <p>Elektron pochta</p>
                  <p>{infoUser.reqCommunication.email}</p>
                  <div className={styles.border}></div>
                </Col>
              </Row>
              <Row>
                <Col md={6} className="d-flex justify-content-end">
                  <button className="btn btn-secondary" onClick={lastPage}>orqaga</button>
                </Col>
                <Col md={6}>
                  <button className="btn btn-danger" onClick={nextPage2}>Tasdiqlayman</button>
                </Col>
              </Row>
            </Container>
          </div> :
          show3 ? <div className={styles.list}>
            <Container className={styles.container}>
              <Row>
                <Col md={4} className="offset-4">
                  <h4 className="text-center mt-4">Ariza topshirish</h4>
                  <div className="line"></div>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col md={4} className="d-flex justify-content-center">
                  <div>
                    <div className="d-flex justify-content-center">
                      <div className={styles.circleActive}><span>1</span></div>
                    </div>
                    <p>Arizani to'ldirish</p>
                    <div></div>
                  </div>
                </Col>
                <Col md={4} className="d-flex justify-content-center">
                  <div>
                    <div className="d-flex justify-content-center">
                      <div className={styles.circle}><span>2</span></div>
                    </div>
                    <p>Yo'nalishni tanlash</p>
                    <div></div>
                  </div>
                </Col>
                <Col md={4} className="d-flex justify-content-center">
                  <div>
                    <div className="d-flex justify-content-center">
                      <div className={styles.circle}><span>3</span></div>
                    </div>
                    <p>Tasdiqlash</p>
                    <div></div>
                  </div>
                </Col>
              </Row>
              <Row className="d-flex justify-content-center mt-5">
                <h5 className="text-center">Ta'lim yo'nalishini tanlash</h5>
              </Row>
              <Row>
                <Col md={12} className="px-5 pb-5">
                  <AvForm onValidSubmit={signUp}>
                    <Label>Yo'nalish</Label>
                    <AvField type="select" name="directionId" placeholder="Iltimos yo'nalishni tanlang">
                      {
                        directions.map(item =>
                          <option key={item.id} value={item.id}>{item.nameUz}</option>
                        )
                      }
                    </AvField>
                    <Label>Ta'lim tili</Label>
                    <AvField type="select" name="languageEducationId" placeholder="Ta'lim tili">
                      {
                        languageEducations.map(item =>
                          <option key={item.id} value={item.id}>{item.nameUz}</option>
                        )
                      }
                    </AvField>
                    <Label>Chet tili</Label>
                    <AvField type="select" name="foreignLanguageId" placeholder="Chet tili">
                      {
                        foreignLanguages.map(item =>
                          <option key={item.id} value={item.id}>{item.nameUz}</option>
                        )
                      }
                    </AvField>
                    <div className="d-flex justify-content-center mt-5">
                      <button type="submit" className="btn btn-danger">Yuborish</button>
                    </div>
                  </AvForm>
                </Col>
              </Row>
              <Row>

              </Row>
            </Container>
          </div> : <div className={styles.list}>
            <Container className={styles.container}>
              <Row>
                <Col md={4} className="offset-4">
                  <h4 className="text-center mt-4">Ariza topshirish</h4>
                  <div className="line"></div>
                </Col>
              </Row>
              <Row className="mt-4">
                <Col md={4} className="d-flex justify-content-center">
                  <div>
                    <div className="d-flex justify-content-center">
                      <div className={styles.circleActive}><span>1</span></div>
                    </div>
                    <p>Arizani to'ldirish</p>
                    <div></div>
                  </div>
                </Col>
                <Col md={4} className="d-flex justify-content-center">
                  <div>
                    <div className="d-flex justify-content-center">
                      <div className={styles.circle}><span>2</span></div>
                    </div>
                    <p>Yo'nalishni tanlash</p>
                    <div></div>
                  </div>
                </Col>
                <Col md={4} className="d-flex justify-content-center">
                  <div>
                    <div className="d-flex justify-content-center">
                      <div className={styles.circle}><span>3</span></div>
                    </div>
                    <p>Tasdiqlash</p>
                    <div></div>
                  </div>
                </Col>
              </Row>
              <Row className="mt-5">
                <Col className="">
                <p>Sizning tizimga kirish uchun login parolingiz:</p>
                <p>
                  <span>Login:</span><span>{infoUser.reqCommunication.phoneNumber}</span>
                </p>
                <p>
                  <span>Parol: </span> <span>{password}</span>
                </p>
                </Col>
              </Row>
              <Row className="mt-5 pb-5">
                <div className="ml-4">
                    <button className="btn btn-danger"><Link className={styles.a} to="/auth/login">Kabenitga kirish</Link></button>
                </div>
              </Row>
            </Container>
          </div>}
      </div>
    );
  }
}

Register.propTypes = {};

export default Register;
