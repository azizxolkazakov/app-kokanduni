import React, {Component} from 'react';
import {connect} from "react-redux";
import { Button,Col,Container,Modal,ModalBody,ModalHeader,Pagination,PaginationItem,PaginationLink,Row} from "reactstrap";
import index from './index.css'
import {AvField, AvForm} from 'availity-reactstrap-validation'

let tempSize = 0;
@connect(({employee}) => ({employee}))

class Employee extends Component {

  componentDidMount() {
    const {dispatch, employee} = this.props;
    dispatch({
      type: 'employee/getEmployee',
    });
  }


  render() {
    const {dispatch, employee} = this.props;
    const {employees, checkItem, check, deleteId, deleteModal,
      checkedId, currentEmployee, openModalA, openModalE,
      paginations, page, size, totalPages
    } = employee;


    const saveEmployee = (e, v) => {
      dispatch({
        type: 'employee/addEmployee',
        payload: {
          ...v
        }
      })
    };
    const openModalDelete = (id) => {
      dispatch({
        type: 'employee/updateState',
        payload: {
          deleteId: id,
          deleteModal: !deleteModal
        }
      })
    };
    const closeModalDelete = () => {
      dispatch({
        type: 'employee/updateState',
        payload: {
          deleteModal: false
        }
      })
    };
    const openModalAdd = () => {
      dispatch({
        type: 'employee/updateState',
        payload: {
          openModalA: !openModalA
        }
      })
    };
    const openModalEdit = (item) => {
      dispatch({
        type: 'employee/updateState',
        payload: {
          openModalE: !openModalE,
          currentEmployee: item
        }
      })
    };
    const deleteEmployee = () => {
      dispatch({
        type: 'employee/deleteEmployee',
        payload: {
          id: deleteId
        }
      })
    };

    const editEmployee = (e, v) => {
      dispatch({
        type: 'employee/editEmployee',
        payload: {
          path: currentEmployee.id,
          ...v
        }
      })
    };


    const checkBox = (e) => {
      if (e.target.checked) {
        checkedId.push(e.target.id);
      }
      if (!e.target.checked) {
        for (let i = 0; i < checkedId.length; i++) {
          if (e.target.id === checkedId[i]) {
            checkedId.splice(i, 1)
          }
        }
      }
      dispatch({
        type: 'employee/updateState',
        payload: {
          checkItem: !checkItem
        }
      });
      console.log(checkedId)
    };
    const deleteEmployees = () => {
      checkedId.map(i =>
        dispatch({
          type: 'employee/deleteEmployee',
          payload: {
            id: i
          }
        })
      );
      dispatch({
        type: 'employee/updateState',
        payload: {
          checkedId: []
        }
      });
      console.log(checkedId)
    };
    const allChecked = (e) => {
      if (e.target.checked) {
        dispatch({
          type: 'employee/updateState',
          payload: {
            check: true
          }
        });
      } else {
        dispatch({
          type: 'employee/updateState',
          payload: {
            check: false
          }
        });
      }
    };

    const selectSize = (e) => {
      tempSize=e.target.value
    };

    const getEmployees=(e,page)=>{
      console.log(page);
      console.log(size);
      dispatch({
        type:'employee/getEmployee',
        payload: {
          page: tempSize > 0 ? page : 0,
          size: tempSize > 0 ? tempSize : size
        }
      })
    };

    return (
      <div>
        <Container>
          <Row className="mt-4 bg-info d-flex align-items-center py-3 text-white">
            <Col md={4}>
              <h2>Manage Employees</h2>
            </Col>
            <Col md={4} className="offset-4">
              <Row>
                <Col md={4} className="pr-0 mr-0">
                  <button className="btn btn-danger pr-4 d-flex ">
                    <div className="d-flex justify-content-center align-items-center" onClick={deleteEmployees}>
                      <div className="ml-2">-</div>
                      <div className="ml-2">Delete</div>
                    </div>
                  </button>
                </Col>
                <Col md={8} className="pl-1">
                  <button className="btn btn-success" onClick={openModalAdd}>
                    <div className="d-flex justify-content-center align-items-center">
                      <div>+</div>
                      <div>Add New Employee</div>
                    </div>
                  </button>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row className="mt-3">
            <div className="col-md-2 offset-10">
              <div>
                <select className="px-3 py-2 option rounded" onChange={selectSize}>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="5">5</option>
                </select>
              </div>
              <Button className="btn-dark mt-2 ml-1" onClick={(e)=>getEmployees(e,page)}>Filter</Button>
            </div>
          </Row>
          <Row>
            <Col md={12}>
              <table className="table   mt-3 border-bottom ">
                <thead>
                <tr>
                  <th className="border-0 px-0 d-flex align-items-center justify-content-center ">
                    <input className={index.checkbox}
                           onChange={allChecked}
                           type="checkbox"
                    />
                  </th>
                  <th className="border-0">Name</th>
                  <th className="border-0">Email</th>
                  <th className="border-0">Address</th>
                  <th className="border-0">Phone</th>
                  <th className="border-0">Actions</th>
                </tr>
                </thead>
                <tbody>
                {employees.map((item) =>
                  <tr key={item.id}>
                    <td className=" d-flex ml-3">
                      <input className={index.checkbox}
                             onChange={checkBox}
                             id={item.id}
                             type="checkbox"
                      />
                    </td>
                    <td>{item.name}</td>
                    <td>{item.email}</td>
                    <td>{item.address}</td>
                    <td>{item.phone}</td>
                    <td>
                      <div className="d-flex justify-content-center">
                        <div className="pr-2">
                          <button className="btn btn-info" onClick={() => openModalEdit(item)}>Edit</button>
                        </div>
                        <div>
                          <button className="btn btn-danger" onClick={() => openModalDelete(item.id)}>Delete</button>
                        </div>
                      </div>
                    </td>
                  </tr>
                )}
                </tbody>

              </table>






              <Modal isOpen={openModalA} toggle={openModalAdd}>
                <ModalHeader>
                  <p>Add Employee</p>
                </ModalHeader>
                <ModalBody>
                  <AvForm onValidSubmit={saveEmployee}>
                    <AvField type="text" placeholder="Enter name" name="name"/>
                    <AvField type="text" placeholder="Enter email" name="email"/>
                    <AvField type="text" placeholder="Enter address" name="address"/>
                    <AvField type="text" placeholder="Enter phone" name="phone"/>
                    <button className="btn btn-success">Save</button>
                  </AvForm>
                </ModalBody>
              </Modal>

              <Modal isOpen={openModalE} toggle={openModalDelete}>
                <ModalHeader>
                  <p>Edit Employee</p>
                </ModalHeader>
                <ModalBody>
                  <AvForm onValidSubmit={editEmployee}>
                    <AvField defaultValue={currentEmployee.name} type="text" placeholder="Enter name" name="name"/>
                    <AvField defaultValue={currentEmployee.email} type="text" placeholder="Enter email" name="email"/>
                    <AvField defaultValue={currentEmployee.address} type="text" placeholder="Enter address"
                             name="address"/>
                    <AvField defaultValue={currentEmployee.phone} type="text" placeholder="Enter phone" name="phone"/>
                    <button className="btn btn-success">Save</button>
                  </AvForm>
                </ModalBody>
              </Modal>

              <Modal isOpen={deleteModal} toggle={openModalDelete}>
                <ModalHeader>
                  <p>Delete Employee</p>
                </ModalHeader>
                <ModalBody>
                  <p>Employee delete?</p>
                  <button className="btn btn-success" onClick={deleteEmployee}>Yes</button>
                  <button className="btn btn-danger" onClick={closeModalDelete}>No</button>
                </ModalBody>
              </Modal>
            </Col>
          </Row>
          <Row className="mt-5 mb-5">
            <Col md={4}>
              <p>Shoving <b>{size}</b> out of <b>{totalPages}</b> entries</p>
            </Col>
            <Col md={4}></Col>
            <Col md={4}  className=" d-flex justify-content-end">
              <Pagination aria-label="Page navigation example">
                <PaginationItem disabled={page === 0}>
                  <PaginationLink previous onClick={(e) => getEmployees(e, page - 1)}>
                    Previous
                  </PaginationLink>
                </PaginationItem>
                {paginations.map((i ) =>
                  <PaginationItem key={i} active={page === (i-1)}
                                  disabled={page === (i - 1) || i === '...'}>
                    <PaginationLink onClick={(e) => getEmployees(e, (i - 1))}>
                      {i}
                    </PaginationLink>
                  </PaginationItem>)}
                <PaginationItem disabled={(totalPages - 1) === page}>
                  <PaginationLink next onClick={(e) => getEmployees(e, page + 1)}>
                    Next
                  </PaginationLink>
                </PaginationItem>
              </Pagination>
            </Col>
          </Row>
        </Container>
      </div>
    );
  }
}

Employee.propTypes = {};

export default Employee;
