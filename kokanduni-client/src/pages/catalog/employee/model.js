import api from 'api'

const {getEmployees,addEmployee,deleteEmployee,editEmployee} = api;


function pagination(page, totalPages) {
  let res = [];
  let from = 1;
  let to = totalPages;
  if (totalPages > 10) {
    from = Math.max(page - 2, 1);
    to = Math.max(Math.min(page + 2, totalPages), 5);
    if (to > 5) {
      res.push(1);
      if (from > 2) res.push(2);
      if (from > 3) {
        if (from === 4) {
          res.push(3);
        } else {
          res.push("...");
        }
      }
    }
  }
  for (let i = from; i <= to; i++) {
    res.push(i);
  }

  if (totalPages > 10) {
    if (to < (totalPages - 2)) {
      if (to === 8) {
        res.push(9);
      } else {
        res.push("...");
      }
    }
    if (to < totalPages)
      res.push(totalPages - 1);
    if (to !== totalPages)
      res.push(totalPages);
  }
  return res;
}

export default ({
  namespace: 'employee',
  state: {
    employees: [],
    openModal:false,
    openModalA:false,
    openModalE:false,
    deleteModal:false,
    deleteId:'',
    currentEmployee:{},
    checkedId:[],
    itemEmployee:{},
    check:false,
    checkItem:false,
    page: 0,
    size: 4,
    totalPages: 0,
    paginations: [],

  },
  subscriptions: {},
  effects: {

    * getEmployee({payload}, {call, put, select}) {
      const {page,size}=yield select(_=>_.employee);
      console.log(payload);
      if (!payload){
        payload={page,size};
      }
      const res=yield call(getEmployees,payload);
      console.log(res);
      if (res.success){
        yield put({
          type:'updateState',
          payload:{
            page:res.page.number,
            employees: res._embedded.employees,
            totalPages: res.page.totalPages,
            paginations: pagination(res.page.number, res.page.totalPages)
          }
        })
      }
    },

    *addEmployee({payload}, {call, put, select}){
     const res= yield call(addEmployee,payload);
      if (res.success){
        yield put({
          type:'updateState',
          payload:{
            openModalA: false
        }
        });
        yield put({
          type:'getEmployee'
        })
      }
    },
    *editEmployee({payload}, {call, put, select}){
      const res=yield call(editEmployee,payload);
      if (res.success){
        yield put({
          type:'updateState',
          payload:{
            currentEmployee: {},
            openModalE:false
          }
        });
        yield put({
          type:'getEmployee'
        })
      }
    },
    *deleteEmployee({payload}, {call, put, select}){
      const res=yield call(deleteEmployee,payload);
      if (res.success){
        yield put({
          type:'updateState',
          payload:{
            deleteModal: false,
            deleteId: ''
          }
        });
        yield put({
          type:'getEmployee'
        })
      }
    }
  },
  reducers: {
    updateState(state, {payload}) {
      return {
        ...state,
        ...payload
      }
    }
  }
})
