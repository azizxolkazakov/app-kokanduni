export default {
  signUp: 'POST /auth/register',
  signIn: 'POST /auth/login',
  userMe: '/employee/me',
  exportPdf:'/auth/exportPdf',

  addPhoto:'POST /attach',
  getPhoto:'/attach',

  getCountries:'/country',
  getCountry:'/country',

  getRegions:'/region',
  getRegion:'/region',

  getDistricts:'/district',
  getDistrict:'/district',

  getGenders:'/gender',
  getGender:'/gender',

  getNations:'/nation',
  getNation:'/nation',

  getCareers:'/career',
  getCareer:'/career',

  getTypeStudies:'/typeStudy',
  getTypeStudy:'/typeStudy',

  getDirections:'/direction',
  getDirection:'/direction',

  getLanguageEducations:'/languageEducation',
  getLanguageEducation:'/languageEducation',

  getForeignLanguages:'/foreignLanguage',
  getForeignLanguage:'/foreignLanguage',
  getEmployees:'/employee',
  addEmployee:'POST /employee',
  deleteEmployee:'DELETE /employee',
  editEmployee:'PUT /employee',

}

