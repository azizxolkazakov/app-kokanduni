package uz.pdp.kokanduni;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KokanduniServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(KokanduniServerApplication.class, args);
    }

}
