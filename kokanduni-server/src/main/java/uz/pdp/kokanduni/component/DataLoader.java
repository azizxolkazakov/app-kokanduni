package uz.pdp.kokanduni.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.kokanduni.entity.User;
import uz.pdp.kokanduni.repository.RoleRepository;
import uz.pdp.kokanduni.repository.UserRepository;


@Component
public class DataLoader implements CommandLineRunner {
    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;
    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public void run(String... args) throws Exception {
        if (initialMode.equals("always")) {
            userRepository.save(
                    new User(
                            "Aziz",
                            "Xolkazakov",
                            "+998934343454",
                            passwordEncoder.encode("0099"),
                            roleRepository.findAll()
                    )
            );
        }
    }
}
