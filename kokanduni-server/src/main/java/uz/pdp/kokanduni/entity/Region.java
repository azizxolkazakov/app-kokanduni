package uz.pdp.kokanduni.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uz.pdp.kokanduni.entity.template.AbsNameEntity;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Region extends AbsNameEntity {
    @ManyToOne(fetch = FetchType.LAZY)
    private Country country;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "region", cascade = CascadeType.ALL)
    private List<District> districts;
    @OneToMany(mappedBy = "region", cascade = CascadeType.ALL)
    private List<PassportInfo> passportInfos;
}
