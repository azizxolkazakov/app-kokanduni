package uz.pdp.kokanduni.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.kokanduni.entity.Employee;


import java.util.UUID;

@Projection(name = "customEmployee",types = Employee.class)
public interface CustomEmployee {
    UUID getId();
    String getName();
    String getEmail();
    String getAddress();
    String getPhone();
    boolean getState();

}
