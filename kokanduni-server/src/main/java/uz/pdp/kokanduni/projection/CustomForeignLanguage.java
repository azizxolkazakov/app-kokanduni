package uz.pdp.kokanduni.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.kokanduni.entity.ForeignLanguage;
import uz.pdp.kokanduni.entity.LanguageEducation;

@Projection(name = "customForeignLanguage", types = ForeignLanguage.class)
public interface CustomForeignLanguage {
    Integer getId();
    String getNameUz();

    String getNameRu();

    String getNameEn();
}
