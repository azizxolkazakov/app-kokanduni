package uz.pdp.kokanduni.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.kokanduni.entity.Direction;
import uz.pdp.kokanduni.entity.LanguageEducation;

@Projection(name = "customLanguageEducation", types = LanguageEducation.class)
public interface CustomLanguageEducation {
    Integer getId();
    String getNameUz();

    String getNameRu();

    String getNameEn();
}
