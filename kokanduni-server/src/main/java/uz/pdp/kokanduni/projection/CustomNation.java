package uz.pdp.kokanduni.projection;

import org.springframework.data.rest.core.config.Projection;
import uz.pdp.kokanduni.entity.Nation;

@Projection(name = "customNation", types = Nation.class)
public interface CustomNation {
    Integer getId();

    String getNameUz();

    String getNameRu();

    String getNameEn();
}
