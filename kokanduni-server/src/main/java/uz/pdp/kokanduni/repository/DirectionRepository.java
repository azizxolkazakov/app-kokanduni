package uz.pdp.kokanduni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.kokanduni.entity.Direction;
import uz.pdp.kokanduni.entity.Gender;
import uz.pdp.kokanduni.projection.CustomDirection;
import uz.pdp.kokanduni.projection.CustomGender;

@RepositoryRestResource(path = "/direction",excerptProjection = CustomDirection.class)
public interface DirectionRepository extends JpaRepository<Direction,Integer> {
}
