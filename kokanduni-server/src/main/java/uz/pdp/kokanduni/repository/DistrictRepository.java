package uz.pdp.kokanduni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import uz.pdp.kokanduni.entity.District;
import uz.pdp.kokanduni.projection.CustomDistrict;

import java.util.List;

@RepositoryRestResource(path = "/district", excerptProjection = CustomDistrict.class)
public interface DistrictRepository extends JpaRepository<District, Integer> {

    @RestResource(path = "/filterByRegion")
    List<District> findAllByRegionId(@Param("id") Integer id);
}
//District data-rest