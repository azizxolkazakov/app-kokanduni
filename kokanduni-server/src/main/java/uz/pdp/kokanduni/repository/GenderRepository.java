package uz.pdp.kokanduni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.kokanduni.entity.Gender;
import uz.pdp.kokanduni.projection.CustomGender;

@RepositoryRestResource(path = "/gender",excerptProjection = CustomGender.class)
public interface GenderRepository extends JpaRepository<Gender,Integer> {
}
