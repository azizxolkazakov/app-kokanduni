package uz.pdp.kokanduni.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.kokanduni.entity.Career;
import uz.pdp.kokanduni.entity.LanguageEducation;
import uz.pdp.kokanduni.projection.CustomCareer;
import uz.pdp.kokanduni.projection.CustomLanguageEducation;

@RepositoryRestResource(path = "/languageEducation",excerptProjection = CustomLanguageEducation.class)
public interface LanguageEducationRepository extends JpaRepository<LanguageEducation, Integer> {
}
