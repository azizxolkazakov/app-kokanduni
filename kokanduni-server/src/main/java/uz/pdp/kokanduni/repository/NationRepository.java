package uz.pdp.kokanduni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.kokanduni.entity.Nation;
import uz.pdp.kokanduni.projection.CustomNation;

@RepositoryRestResource(path = "/nation",excerptProjection = CustomNation.class)
public interface NationRepository extends JpaRepository<Nation, Integer> {
}
