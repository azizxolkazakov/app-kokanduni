package uz.pdp.kokanduni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.kokanduni.entity.Region;
import uz.pdp.kokanduni.projection.CustomRegion;


@RepositoryRestResource(path = "/region", excerptProjection = CustomRegion.class)
public interface RegionRepository extends JpaRepository<Region, Integer> {

}
