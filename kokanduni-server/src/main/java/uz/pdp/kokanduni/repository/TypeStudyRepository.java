package uz.pdp.kokanduni.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.kokanduni.entity.TypeStudy;
import uz.pdp.kokanduni.projection.CustomTypeStudy;

@RepositoryRestResource(path = "/typeStudy",excerptProjection = CustomTypeStudy.class)
public interface TypeStudyRepository extends JpaRepository<TypeStudy, Integer> {
}
