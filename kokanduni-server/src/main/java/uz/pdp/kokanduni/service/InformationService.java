package uz.pdp.kokanduni.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.pdp.kokanduni.entity.Information;
import uz.pdp.kokanduni.payload.ReqInformation;
import uz.pdp.kokanduni.repository.*;

@Service
public class InformationService {
    @Autowired
    CareerRepository careerRepository;
    @Autowired
    TypeStudyRepository typeStudyRepository;
    @Autowired
    RegionRepository regionRepository;
    @Autowired
    DistrictRepository districtRepository;
    @Autowired
    InformationRepository informationRepository;

    public Information addInformation(ReqInformation reqInformation) {
        Information information = new Information();
        information.setCareer(careerRepository.getOne(reqInformation.getCareerId()));
        information.setTypeStudy(typeStudyRepository.getOne(reqInformation.getTypeStudyId()));
        information.setStudyName(reqInformation.getStudyName());
        information.setEndStudyYear(reqInformation.getEndStudyYear());
        information.setRegion(regionRepository.getOne(reqInformation.getRegionIdInfo()));
        information.setDistrict(districtRepository.getOne(reqInformation.getDistrictIdInfo()));
        return informationRepository.save(information);
    }
}
