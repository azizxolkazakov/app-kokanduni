insert into role values (10,'ROLE_USER'),(20,'ROLE_MANAGER'),(30,'ROLE_DIRECTOR'),(40,'ROLE_ADMIN');

INSERT INTO gender(name_uz, name_ru,name_en) VALUES ('Erkak', 'Mужской','Male'),('Ayol', 'Женщина','Female');
insert into country(name_uz, name_ru,name_en) values ('O`zbekiston','O`zbekiston','O`zbekiston');
insert into country(name_uz, name_ru,name_en) values ('Qozog`iston','Qozog`iston','Qozog`iston');
insert into country(name_uz, name_ru,name_en) values ('Tojikiston','Tojikiston','Tojikiston');
insert into country(name_uz, name_ru,name_en) values ('Turkmaniston','Turkmaniston','Turkmaniston');
insert into country(name_uz, name_ru,name_en) values ('Qirg`iziston','Qirg`iziston','Qirg`iziston');

insert into nation(name_uz, name_ru,name_en) values ('O`zbek','O`zbek','O`zbek'),('Rus','Rus','Rus'),('Qozoq','Qozoq','Qozoq');
insert into career(name_uz, name_ru,name_en) values('Oliy','Oliy','Oliy'),('O`rta','O`rta','O`rta'),('Quyi','Quyi','Quyi');
insert into type_study(name_uz, name_ru,name_en) values('Litsey','Litsey','Litsey'),('Kollej','Kollej','Kollej'),('Maktab','Maktab','Maktab');
insert into direction(name_uz, name_ru,name_en) values('Moliya','Moliya','Moliya'),('Iqtisod','Iqtisod','Iqtisod'),('IT','IT','It'),('Meditsina','Meditsina','Meditsina');
insert into language_education(name_uz, name_ru,name_en) values ('O`zbek','O`zbek','O`zbek'),('Rus','Rus','Rus'),('Qozoq','Qozoq','Qozoq');
insert into foreign_language(name_uz, name_ru,name_en) values('English','English','English'),('Rus','Rus','Rus'),('Fransuz','Fransuz','Fransuz');
-- Viloyatlar
insert into region(country_id,name_uz, name_ru,name_en) values (1,'ТОШКЕНТ ШАҲАР', 'ТОШКЕНТ ШАҲАР','ТОШКЕНТ ШАҲАР');
insert into region(country_id,name_uz, name_ru,name_en) values (1,'ТОШКЕНТ ВИЛОЯТИ', 'ТОШКЕНТ ВИЛОЯТИ','ТОШКЕНТ ВИЛОЯТИ');
insert into region(country_id,name_uz, name_ru,name_en) values (1,'АНДИЖОН ВИЛОЯТИ', 'АНДИЖОН ВИЛОЯТИ','АНДИЖОН ВИЛОЯТИ');
insert into region(country_id,name_uz, name_ru,name_en) values (1,'БУХОРО ВИЛОЯТИ', 'БУХОРО ВИЛОЯТИ','БУХОРО ВИЛОЯТИ');
insert into region(country_id,name_uz, name_ru,name_en) values (1,'ЖИЗЗАХ ВИЛОЯТИ', 'ЖИЗЗАХ ВИЛОЯТИ','ЖИЗЗАХ ВИЛОЯТИ');
insert into region(country_id,name_uz, name_ru,name_en) values (1,'ҚАШҚАДАРЁ ВИЛОЯТИ', 'ҚАШҚАДАРЁ ВИЛОЯТИ','ҚАШҚАДАРЁ ВИЛОЯТИ');
insert into region(country_id,name_uz, name_ru,name_en) values (1,'НАВОИЙ ВИЛОЯТИ', 'НАВОИЙ ВИЛОЯТИ','НАВОИЙ ВИЛОЯТИ');
insert into region(country_id,name_uz, name_ru,name_en) values (1,'НАМАНГАН ВИЛОЯТИ', 'НАМАНГАН ВИЛОЯТИ','НАМАНГАН ВИЛОЯТИ');
insert into region(country_id,name_uz, name_ru,name_en) values (1,'САМАРҚАНД ВИЛОЯТИ', 'САМАРҚАНД ВИЛОЯТИ','САМАРҚАНД ВИЛОЯТИ');
insert into region(country_id,name_uz, name_ru,name_en) values (1,'СУРХОНДАРЁ ВИЛОЯТИ', 'СУРХОНДАРЁ ВИЛОЯТИ','СУРХОНДАРЁ ВИЛОЯТИ');
insert into region(country_id,name_uz, name_ru,name_en) values (1,'СИРДАРЁ ВИЛОЯТИ', 'СИРДАРЁ ВИЛОЯТИ','СИРДАРЁ ВИЛОЯТИ');
insert into region(country_id,name_uz, name_ru,name_en) values (1,'ФАРҒОНА ВИЛОЯТИ', 'ФАРҒОНА ВИЛОЯТИ','ФАРҒОНА ВИЛОЯТИ');
insert into region(country_id,name_uz, name_ru,name_en) values (1,'ХОРАЗМ ВИЛОЯТИ', 'ХОРАЗМ ВИЛОЯТИ','ХОРАЗМ ВИЛОЯТИ');
insert into region(country_id,name_uz, name_ru,name_en) values (1,'ҚОРАҚАЛПОҒИСТОН РЕСП', 'ҚОРАҚАЛПОҒИСТОН РЕСП','ҚОРАҚАЛПОҒИСТОН РЕСП');


insert into district(region_id,name_uz, name_ru,name_en) values (1,'Yakkasaroy tumani','Yakkasaroy tumani','Yakkasaroy tumani');
insert into district(region_id,name_uz, name_ru,name_en) values (7,'Karmana tumani','Karmana tumani','Karmana tumani');
insert into district(region_id,name_uz, name_ru,name_en) values (1,'Yunusobod tumani','Yunusobod tumani','Yunusobod tumani');
insert into district(region_id,name_uz, name_ru,name_en) values (1,'Chilonzor tumani','Chilonzor tumani','Chilonzor tumani');



--To'lov turlarini qo'shish